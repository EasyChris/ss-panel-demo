{include file='v2/header.tpl'}

<header>
    <div class="header-content">
        <div class="header-content-inner">
            <h1 id="homeHeading">Big world , Awaiting explored.</h1>
            <hr>
            <p>There’s so much in the world that i’d like to soak up with my eyes.</p>
            <a href="/auth/login" class="btn btn-primary btn-xl page-scroll">Sign in</a>
        </div>
    </div>
</header>

<section class="bg-primary" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading">快速 · 稳定 · 实惠</h2>
                <hr class="light">
                <p class="text-faded">我们的所有节点全部架设于云服务，且均由监控系统每15 秒扫描一次节点，任何节点故障时，系统将自动启用备用设备，尽最大可能不影响用户使用。</p>
                <a href="#services" class="page-scroll btn btn-default btn-xl sr-button">现在开始使用</a>
            </div>
        </div>
    </div>
</section>

<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">At Your Service</h2>
                <hr class="primary">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box">
                    <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
                    <h3>优选节点</h3>
                    <p class="text-muted">全球最优线路节点，最新TCP算法加速，多条线路供选择切换。</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box">
                    <i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i>
                    <h3>独立账号</h3>
                    <p class="text-muted">独立账号，独立端口，一人一号，从根源上保护你的上网安全</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box">
                    <i class="fa fa-4x fa-money text-primary sr-icons"></i>
                    <h3>精确计费</h3>
                    <p class="text-muted">多种付费选择。可以选择用多少付多少，或者包月用到爽。</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box">
                    <i class="fa fa-4x fa-heart text-primary sr-icons"></i>
                    <h3>多端支持</h3>
                    <p class="text-muted">跨平台支持IOS、Android、PC，实现无缝切换。</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-primary" id="about-price">
    <div class="container">
        <div class="row">
            {foreach $packageArr as $package}
                {assign var=info value=$package->info|json_decode:1}
                <div class="col-lg-4 col-sm-6 text-center">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="price-title">
                                {$package.name}
                                <div class="price-p">
                                    {$package->price}元 /
                                    {if $info.duration/30 > 11}
                                        年
                                    {else}
                                        {($info.duration/30)|round}个月
                                    {/if}
                                </div>
                            </div>
                            {assign var=info value=$package->info|json_decode:1}
                            <div class="price-info">
                                <ul class="price-info-p">
                                    <li>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        国际 高速 线路
                                    </li>
                                    <li>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        私有 独立帐号及密码
                                    </li>
                                    <li>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        {if $info.bandwidthCheck eq 'true'}
                                            <strong style="color: #333">全时段最高 {$info.bandwidth} 带宽</strong>
                                        {else}
                                            全时段最高 {$info.bandwidth} 带宽
                                        {/if}
                                    </li>
                                    <li>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        {if $info.flowCheck eq 'true' }
                                            <strong style="color: #333">{$info.flow} G/月 使用流量</strong>
                                        {else}
                                            {$info.flow} G/月 使用流量
                                        {/if}
                                    </li>
                                    <li>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        多个可用节点
                                    </li>
                                    <li>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        支持LotServer加速
                                    </li>
                                    <li>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        日本、美国、香港等多节点可用
                                    </li>
                                </ul>
                                <button class="price-btn">立即购买</button>
                            </div>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</section>

<section class="no-padding" id="portfolio">
    <div class="container-fluid">
        <div class="row no-gutter popup-gallery">
            <div class="col-lg-4 col-sm-6">
                <a href="https://ooo.0o0.ooo/2017/02/03/5894763f927ba.png" class="portfolio-box">
                    <img src="https://ooo.0o0.ooo/2017/02/03/5894763f927ba.png" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <!-- <div class="project-category text-faded">
                                Category
                            </div> -->
                            <div class="project-name">
                                Google
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="https://ooo.0o0.ooo/2017/02/03/5894763fb2647.png" class="portfolio-box">
                    <img src="https://ooo.0o0.ooo/2017/02/03/5894763fb2647.png" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-name">
                                Facebook
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="https://ooo.0o0.ooo/2017/02/03/5894763fc8ee9.png" class="portfolio-box">
                    <img src="https://ooo.0o0.ooo/2017/02/03/5894763fc8ee9.png" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-name">
                                Twitter
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="https://ooo.0o0.ooo/2017/02/03/5894763fd34ba.png" class="portfolio-box">
                    <img src="https://ooo.0o0.ooo/2017/02/03/5894763fd34ba.png" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-name">
                                Gmail
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="https://ooo.0o0.ooo/2017/02/03/5894763fd3d02.png" class="portfolio-box">
                    <img src="https://ooo.0o0.ooo/2017/02/03/5894763fd3d02.png" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-name">
                                Github
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="https://ooo.0o0.ooo/2017/02/03/5894763fd45da.png" class="portfolio-box">
                    <img src="https://ooo.0o0.ooo/2017/02/03/5894763fd45da.png" class="img-responsive" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-name">
                                Youtube
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<aside class="bg-dark">
    <div class="container text-center">
        <div class="call-to-action">
            <h2>Free Start at Gocyc!</h2>
            <a href="http://startbootstrap.com/template-overviews/creative/" class="btn btn-default btn-xl sr-button">Register
                Now!</a>
        </div>
    </div>
</aside>

<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading">Gocyc Shadowsocks</h2>
                <hr class="primary">
                <p>目前最快、最稳定、最智能的跨域科学上网方式，是替代传统VPN的最佳选择！</p>
            </div>
            <div class="col-lg-12 text-center">
                <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                <p><a href="mailto:webgocyc@gmail.com">webgocyc@gmail.com</a></p>
            </div>
        </div>
    </div>
</section>

{include file='v2/footer.tpl'}