
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <p>Copyright 2016 © gocyc.cc. All rights reserved.</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="http://cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="http://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="http://cdn.bootcss.com/scrollReveal.js/3.3.2/scrollreveal.min.js"></script>
<script src="http://cdn.bootcss.com/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

<!-- Theme JavaScript -->
<script src="assets/v2/js/creative.min.js"></script>

</body>

</html>