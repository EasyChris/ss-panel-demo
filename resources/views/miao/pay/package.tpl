{include file='../user/main.tpl'}
            <div class="content-wrapper package-container">
                <div class="row">
                    {foreach $packageArr as $package}
                        {assign var=info value=$package->info|json_decode:1}
                    <div class="col-lg-4 col-sm-6 text-center">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="price-title">
                                    {$package->name}
                                    <div class="price-p">
                                        {$package->price}元 /
                                        {if $info.duration/30 > 11}
                                            年
                                        {else}
                                            {($info.duration/30)|round}个月
                                        {/if}
                                    </div>
                                </div>

                                <div class="price-info">
                                    <ul class="price-info-p">
                                        <li>
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            国际 高速 线路
                                        </li>
                                        <li>
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            私有 独立帐号及密码
                                        </li>
                                        <li>
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            {if $info.bandwidthCheck eq 'true'}
                                                <strong>全时段最高 {$info.bandwidth}M带宽</strong>
                                            {else}
                                                全时段最高 {$info.bandwidth} 带宽
                                            {/if}
                                        </li>
                                        <li>
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            {if $info.flowCheck eq 'true' }
                                            <strong>{$info.flow} G/月 使用流量</strong>
                                            {else}
                                            {$info.flow} G/月 使用流量
                                            {/if}
                                        </li>
                                        <li>
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            多个可用节点
                                        </li>
                                        <li>
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            支持LotServer加速
                                        </li>
                                        <li>
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            日本、美国、香港等多节点可用
                                        </li>
                                    </ul>
                                    <button class="price-btn">立即购买</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/foreach}
                    {*<div class="col-lg-4 col-sm-6 text-center">*}
                        {*<div class="panel panel-default">*}
                            {*<div class="panel-body">*}
                                {*<div class="price-title">*}
                                    {*包季套餐*}
                                    {*<div class="price-p">*}
                                        {*60元/季*}
                                    {*</div>*}
                                {*</div>*}

                                {*<div class="price-info">*}
                                    {*<ul class="price-info-p">*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*国际 高速 线路*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*私有 独立帐号及密码*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*<strong>全时段最高 50M 带宽</strong>*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*<strong>100.0 G/月 使用流量</strong>*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*多个可用节点*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*支持LotServer加速*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*日本、美国、香港等多节点可用*}
                                        {*</li>*}
                                    {*</ul>*}
                                    {*<button class="price-btn">立即购买</button>*}
                                {*</div>*}
                            {*</div>*}
                        {*</div>*}
                    {*</div>*}
                    {*<div class="col-lg-4 col-sm-6 text-center">*}
                        {*<div class="panel panel-default">*}
                            {*<div class="panel-body">*}
                                {*<div class="price-title">*}
                                    {*包年套餐*}
                                    {*<div class="price-p">*}
                                        {*200元/年*}
                                    {*</div>*}
                                {*</div>*}

                                {*<div class="price-info">*}
                                    {*<ul class="price-info-p">*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*国际 高速 线路*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*私有 独立帐号及密码*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*<strong>全时段最高 100M 带宽</strong>*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*<strong>200.0 G/月 使用流量</strong>*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*多个可用节点*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*支持LotServer加速*}
                                        {*</li>*}
                                        {*<li>*}
                                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                            {*日本、美国、香港等多节点可用*}
                                        {*</li>*}
                                    {*</ul>*}
                                    {*<button class="price-btn">立即购买</button>*}
                                {*</div>*}
                            {*</div>*}
                        {*</div>*}
                    {*</div>*}
                </div>
            </div>
{include file='../user/footer.tpl'}