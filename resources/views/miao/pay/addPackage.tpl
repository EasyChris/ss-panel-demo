{include file='../admin/main.tpl'}
<div class="content-wrapper package-container">
    <div class="admin-package-add">
        <div class="row">
            <div class="col-lg-12 col-sm-6 form-group">
                <label for="title">套餐名字</label>
                <input type="text" id="p-title" placeholder="请输入套餐名字" name="name" class="form-control">
            </div>
            <div class="col-lg-12 col-sm-6 form-group">
                <label for="p-price">套餐价格</label>
                <input type="text" id="p-price" placeholder="输入套餐价格，单位元" name="price" class="form-control">
                <label for="p-price-check">加粗</label>
                <input type="checkbox" id="p-price-check" name="priceCheck">
            </div>
            <div class="col-lg-12 col-sm-6 form-group">
                <label for="p-duration">套餐天数</label>
                <input type="text" id="p-duration" placeholder="输入套餐天数，单位天" name="duration" class="form-control">
                <label for="p-duration-check">加粗</label>
                <input type="checkbox" id="p-duration-check" name="durationCheck">
            </div>
            <div class="col-lg-12 col-sm-6 form-group">
                <label for="p-bandwidth">最高带宽</label>
                <input type="text" id="p-bandwidth" placeholder="输入账户带宽，单位M" name="bandwidth" class="form-control">
                <label for="p-bandwidth-check">加粗</label>
                <input type="checkbox" id="p-bandwidth-check" name="bandwidthCheck">
            </div>
            <div class="col-lg-12 col-sm-6 form-group">
                <label for="p-flow">流量额度</label>
                <input type="text" id="p-flow" placeholder="输入套餐带宽，单位G" name="flow" class="form-control">
                <label for="p-flow-check">加粗</label>
                <input type="checkbox" id="p-flow-check" name="flowCheck">
            </div>
        </div>
        <button type="submit" class="btn btn-default packageBtn">增加套餐</button>
    </div>
</div>
<script src="/assets/public/js/tool.js"></script>
<script>
    $(function () {
        $('.packageBtn').click(function () {
            var inputArr = document.querySelectorAll('.form-control');
            inputArr.forEach(function (e, index, array) {
                if (!is_empty(e.value)) {
                    alert('输入框' + (index + 1) + '不能为空');
                    return false;
                }
            });
            $.ajax({
                url: '/admin/package/add',
                type: "POST",
                data: {
                    name:getEle('#p-title').value,
                    price:getEle('#p-price').value,
                    duration:getEle('#p-duration').value,
                    bandwidth:getEle('#p-bandwidth').value,
                    flow:getEle('#p-flow').value,
                    priceCheck:getEle('#p-price-check').checked,
                    durationCheck:getEle('#p-duration-check').checked,
                    bandwidthCheck:getEle('#p-bandwidth-check').checked,
                    flowCheck:getEle('#p-flow-check').checked,
                },
                success:function (res) {
                    var resObj = JSON.parse(res);
                    if (resObj.code == 200) {
                        alert(resObj.message);
                        location.href = '/admin';
                    }else{
                        alert(resObj.message);
                    }
                }
            }).done(function( html ) {

            });
        })
    });
</script>
{include file='../user/footer.tpl'}