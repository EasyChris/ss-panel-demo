<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 2017/5/31
 * Time: 22:42
 */
namespace App\Controllers;

use App\Models\Shop;
use Slim\Http\Request;
use Slim\Http\Response;

use App\Models\InviteCode;
use App\Services\Auth;
use App\Services\Config;
use App\Services\DbConfig;
use App\Services\Logger;
use App\Utils\Check;
use App\Utils\Http;


class PayController extends BaseController{

    private $user;

    public function __construct()
    {
        $this->user = Auth::getUser();
    }

    public function view()
    {
        $userFooter = DbConfig::get('user-footer');
        return parent::view()->assign('userFooter', $userFooter);
    }

    /**
     * 选择套餐
     */
    public function package()
    {
        $shop = new Shop();
        $packageArr = $shop::all();
        return $this->view()->assign('packageArr',$packageArr)->display('pay/package.tpl');
    }

    /**
     * 支付控制器
     */
    public function pay()
    {
        return $this->view()->display('pay/index.tpl');
    }

}