<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 2017/6/4
 * Time: 18:04
 */

namespace App\Controllers\Admin;

use App\Controllers\AdminController;
use App\Models\Shop;

class PackageController extends AdminController
{
    public function index($request, $response, $args)
    {
//        $shop = new Shop();
//        $packageArr = $shop::all();
//        var_dump($packageArr);
//        exit();
        return $this->view()->display('pay/addPackage.tpl');
    }

    public function add($request, $response, $args)
    {
        $packageArr = $request->getParsedBody();
        $packJson = json_encode($packageArr);
        $shop = new Shop();
        $shop->name = $packageArr['name'];
        $shop->price = $packageArr['price'];
        $shop->duration = $packageArr['duration'];
        $shop->info = $packJson;
        $shop->create_time = time();
        if($shop->save()) {
            $res = [
                'code' => 200,
                'message' => '添加成功'
            ];
        }else{
            $res = [
                'code' => 400,
                'message' => '添加失败',
            ];
        }
        return json_encode($res);
    }
}