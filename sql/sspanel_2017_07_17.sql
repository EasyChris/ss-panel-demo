/*
Navicat MySQL Data Transfer

Source Server         : homestead
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : sspanel

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-07-18 00:00:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sp_config
-- ----------------------------
DROP TABLE IF EXISTS `sp_config`;
CREATE TABLE `sp_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sp_config
-- ----------------------------

-- ----------------------------
-- Table structure for sp_email_verify
-- ----------------------------
DROP TABLE IF EXISTS `sp_email_verify`;
CREATE TABLE `sp_email_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(32) NOT NULL,
  `token` varchar(64) NOT NULL,
  `expire_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sp_email_verify
-- ----------------------------

-- ----------------------------
-- Table structure for sp_log
-- ----------------------------
DROP TABLE IF EXISTS `sp_log`;
CREATE TABLE `sp_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(16) NOT NULL,
  `msg` text NOT NULL,
  `created_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sp_log
-- ----------------------------

-- ----------------------------
-- Table structure for ss_checkin_log
-- ----------------------------
DROP TABLE IF EXISTS `ss_checkin_log`;
CREATE TABLE `ss_checkin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `checkin_at` int(11) NOT NULL,
  `traffic` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ss_checkin_log
-- ----------------------------

-- ----------------------------
-- Table structure for ss_invite_code
-- ----------------------------
DROP TABLE IF EXISTS `ss_invite_code`;
CREATE TABLE `ss_invite_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ss_invite_code
-- ----------------------------
INSERT INTO `ss_invite_code` VALUES ('1', '123gFjDLWeppkCitENXLumDCxWVxXcBtUiS', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('3', '12383n3Dp7ikYwYvPVBBASDhXTfkFMJTs9m', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('4', '123mt6xKxGfWjPFhVfmfifZAncsPj9HGpPM', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('5', '123FyixtXKdvVZfzqcKBgqjqkdUbECRt9bG', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('6', '123xvfpCwKGUhe5u2VuxHiA5sde4pnrmNrv', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('7', '123pNcnZdhEu6tk5Ms2CbzjSwHvLBMSCnUv', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('8', '123AbZFcNSGbjfUZW4iXQe4x28Db5C2wsGS', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('9', '123pCFyXrhAjA3YLHtjJcqasshwt9cjqccZ', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('10', '123sd3MEgmaa7EWjfYGSuc2wQL7P9bTkuZR', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('11', '123hAXWLhJHwmaCMWKuAZWB7Z5XcbwzuwbM', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('12', '1235Ndphsw8mnHB6Vkx7URNTWUwByWyqhKe', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('13', '123GirYfsxw6Cncyz9kpFEYEBr2E75WiTBu', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('14', '123w2uF4LLbZAUNpxYfLftffxJfksRwSWei', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('15', '12354t826XVXVkArPXRt5uv3XqR3nuWf85F', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('16', '123WpPPsKLAM29Rx6W7uwrZupgRFeaZYuk7', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('17', '123d2mgT6RjbNTjjWAY2VbmKK3vr89QrQAM', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('18', '123aGw8T6NfsSXtJ93MVu4wecaGcDGvfnvE', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('19', '123yVqeRLX3FdWCMXT7yyUt2PWCpPTxaTgf', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('20', '123RshG57nYXeBiB2Dy9whNRaAjQenNxCEM', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('21', '1237P4tEMFUjda4BtkXqWJ6FCZLqa4BWG7r', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('22', '1237ArmbigqjYqtGnMpXrjVan24fspTxcri', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('23', '123XiCMCn69ukJD8QfXUHfCE6ADqaqeQa6d', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('24', '1232t3Rg9mFbKxBPEd6Vz66YqunJSwAM4iz', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('25', '12378L5KEsyDBAanGrrJt7mcUyep6U7k3B4', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('26', '123easRiVrwSnyx4nu4umGuwfFPB68wgf2V', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('27', '123yzQCF6aTcmyZhUJHwyp67rcFYBfhZzdr', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('28', '12358QCVDyRhhupxFg3Ah3fVB7GuZaNWsz9', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('29', '123hxMczFtEGBVHZaUJPFkiC6r7AVHNzwHD', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('30', '123NEzW8WvJ5JwsginTjzwvV566imECVC2X', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('31', '123N2Eayw7FDKfjR6EPTeX89f5h4GD56nez', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('32', '123AHtUPUpgjpfBp2L3HYCAzMKxbeX69nJb', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('33', '123sc9q6TZLvbAQkYbmMbymw3mJ58ie9GnL', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('34', '123JMcgGvjU95wYAZVsFPb6Tc6TyAL6U3MC', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('35', '123KzC2XGQNfG9eAzyKQPMqapCaSckQrNny', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('36', '123THEP63um8wjdGmxzx54vBSJecp8bwXZS', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('37', '123VHDjD3pp8NhtnJqvmEGeenRxZAABBDB9', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('38', '123er3Vzp559LMaJafMhUzgxM7bbW9vQ5Q9', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('39', '123EifQYkMHyySMTSnajv7qzgdTLGLCD2Jm', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('40', '123KRsbWuz9ctL2gMDFGr2vHCj4MZS8jkQ3', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('41', '123TG8tEzdJLjptp8UVyhXnpvmqJic6U4Wj', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('42', '123eHu7AQHx5XT4R2UUEzXvWx7cVFgZpH9i', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('43', '123PJYnNWguXDfZ2KfUfyue6LDDjkJB7nrq', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('44', '123PDC7UsWhpYcFZhDRjw9ybTuJepMBNFZ6', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('45', '123jk8YEpYDjWvgfZjUxZvyw8DKnEi7RD4Q', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('46', '123hnF8PCknAY7KJkkhedr4JvXhqqAKh4qq', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('47', '123SnbPSzYgFYkjGVDeJ5CzYDjCuNsPAJ7a', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('48', '123X2PULvFEFWDGX4DFjC2nemjhxsNmGaDi', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('49', '1239vigbbYHG8PgXg6jezfMTvLJeR2rHfSk', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('50', '123wbLt4k8M7YQhEegsvdzuzQJCFufcgung', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('51', '123WXkZ4fQDBcmsTj7KdhpmeTKcCJ6rpbcH', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('52', '123gZVrnd4NQq5rURGLMhfDssuiPwYwJLE5', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('53', '123ez6AcY97GLtAPaNBst7C4pjKWL8UZ8z6', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('54', '123v4ruP4gJTL8487fetqcj5ghedzyZPjgH', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('55', '123XEyfCZd25K2VsLwMPkU2jRP8ZY8995nk', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('56', '123newG6tLrnTazCZ35tARFMjWAukQDppYh', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('57', '1238zzZ3GS5WnYfgAchPTPJZNGwzkQAyiY6', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('58', '123UBKZziFabj5Rp7XsQ5vP886YDnEMACeV', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('59', '123GfPhDuA9e4KyiDvKM9JDntALWHWGA2qs', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('60', '123Sr7s5pEuKXcBpxn65czYWHfa9PVNU6Y2', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('61', '1237AGT5tG26FaweHKY65UH8R5i4gzAHrEX', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('62', '123sLUDZKdzEVwwVKUuq685J5H2JBbMcrgz', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('63', '1233NGqh3y8a4fhBnLh54v9NCT4PVgLu5yN', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('64', '1238fBAL5tnVKSsLeWNJ5aXEHe7nVvRWcDF', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('65', '1235VDbAxMx4acSEyZKKv2HNnJvjmf3XN9w', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('66', '123GzEkE8jPAJbhHCmfpybArJTErR7nVDS6', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('67', '123GYJwQjdMub5TmWQjEZtzgxm4WrHW6EPd', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('68', '123iRJUNa6A4NQfDjPWWiEEsP9KhszTcU6t', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('69', '123Y9vL6h3L4KKaknsecfCjfPE7BS37TyAB', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('70', '123eXutxLKm4dPdRFZPyuhNT6DQQZ5ZzeKh', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('71', '123YSHsa3YELbEBHsBrcLRTwDGWUwFmaqky', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('72', '123u9mBA94277Z4CUUSr4nz7HUB8pxxKkNg', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('73', '123JpLWMPB4p2kdVBxWtmi8rcDFrgeh7HqJ', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('74', '1234AujY9Dbmx3CcFFnFwRnnEGBtvaRcDVe', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('75', '123PSeDTTJsbVQDm7WXQ3egh73U9nqWHGm3', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('76', '1234GDztcdfdhsXQcsmp7LxpXMCZteGBcri', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('77', '123wWUs6UbThktetk4p2FX3anPXCLJKVcXM', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('78', '123vPQH8E7eNTvgqAHTuUryDhM9NSLDJDt4', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('79', '123LdBNFVfRhbnJWBeDvxzeN2shuuu2Hjgz', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('80', '123m4vSjBMUsKzMJfXfZjKQ6mg5dCsXDUnY', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('81', '123CVnjAVpjBPTFhrXA4Z34DkedDY5k3ji2', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('82', '123JTTL4yGsG4qyrYnpEsLFpBthVsZriyNk', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('83', '123c248csG7dvpnwY36STxXKatCa6KkGGPX', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('84', '1237gj3kV2k7aFmr35nsTwpePVMQmdjKb2a', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('85', '123b7r7WsBK4PUrR5MF4QzimUh49CgmhzP5', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('86', '123z4gMXicGdfWmgAWcqA5ZtdTvy9YTSWKm', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('87', '123ns6xNdBssNJJqTAk7n2LuV7Q8ehL69AP', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('88', '123U2rnLcwxKynJafMwttbQzinSyYRwSquL', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('89', '123Cc7xpJvq3Z7NgXWLfSbzmH7SvYeZNgeS', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('90', '123f2xAKCDLmLTAaMhQN66pPD4QGZymZVm7', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('91', '123B6ERTEyLPAXmXH2xWjympqkanNn6zzbQ', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('92', '123MjQ7TL6WGeLgfJbSz4YsydL6RKA5Nk5S', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('93', '123UQUju49yHAyUZ68RLrbza8WdfgTxhBh5', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('94', '123UFijHUjjbWRFm2iGhJLb6ghDE2juFYWf', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('95', '123Ls8TbFB5g2J9b2dWriFrmM5cTG2cj26Z', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('96', '1233MfDhrMDryk4N69TUkMVJfexUGjEg5Ng', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('97', '123rknt9SMBckjkHMFhjdQp2YVGjbuhTzjN', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('98', '123xkTnzwSsLxRqRsq3PtXrhgrndAjVVJLp', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('99', '123B9hffH7M8ELeWxp97v3hVsY8NResXzvm', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('100', '123tfTVmFXVWwpECyQrLJXN595TPU8ZJrWG', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('101', '123HNyWMvnNWGJf2AdfiKTDskyjim7v6tVT', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('102', '1239TEJinA2dUMYn78T27k3wffqZq3Z2h6B', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('103', '123zSWmyHyKCVpKFTP3bVdsCQLwUPtJD7gv', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('104', '123qYz8V8YHm3DYSpLuaRdaZbsp7Zj6hCia', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('105', '123sAJMbYbqZTGEpVdXqPCc8DSvvLP7NtQx', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('106', '123RZBYVt8zmCepVArHQfyKXMbYfc6ciD5Y', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('107', '123gBuYaFu6wfimSRsf9fLQmTPk5KiAwUKB', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('108', '123gd6XSzQGsXtVyBRHTychbEdY3dWkyP7L', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('109', '123Xs4He9zS3D4iVhzgwjN2mtukRYtcEcXX', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('110', '123EAhUD8BMZbGpsiESqJQgjhzKZiWkW9C2', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('111', '12353zUcQ7gtHDRV3XCMAwhmfKY9sjT7tnn', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('112', '123V8Raicxnzd3tg7RGsFMxXYKjWcvLmtPm', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('113', '1238B8nM8sri92qU6DuJt5PaqSqHzmCXjdG', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('114', '123Z6dGYpHXC34e5Y7vYKtQjqhdbsGcm7ht', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('115', '123zZkbm2GJUGBaAJQp3Nt2CDGxCJL9zRac', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('116', '123zmFZ74yzqkyfyzwv4sGQPtXvNg5yEFXB', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('117', '123TiaiHLcHmkx3YMZ9U9fDfhqNEmpQ4QYz', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('118', '123UDKGASfe9nN9BxZLfKWjnpmWzDeXvAGr', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('119', '123xM9vqq9UVHRLjtZFHzC5LcxXQKUXbzjj', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('120', '12329WExksFfdNcvGNuPVLHN8zjUWhwqtMe', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('121', '123FqkLCjvVL2qHHcaTR8R8KEXsUsR8mfez', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('122', '123yC5wN6tSHMNh66GFZDMLfNAGUeHRsnpz', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');
INSERT INTO `ss_invite_code` VALUES ('123', '123B5AtcAunbkjZdgT3SHrjB5ZZCkxfnk3m', '0', '2017-07-16 09:41:21', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for ss_node
-- ----------------------------
DROP TABLE IF EXISTS `ss_node`;
CREATE TABLE `ss_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `type` int(3) NOT NULL,
  `server` varchar(128) NOT NULL,
  `method` varchar(64) NOT NULL,
  `custom_method` tinyint(1) NOT NULL DEFAULT '0',
  `traffic_rate` float NOT NULL DEFAULT '1',
  `info` varchar(128) NOT NULL,
  `status` varchar(128) NOT NULL,
  `offset` int(11) NOT NULL DEFAULT '0',
  `sort` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ss_node
-- ----------------------------
INSERT INTO `ss_node` VALUES ('1', '1', '1', 'hk1.gocyc.cc', 'rc4-md5', '0', '1', '', '1', '0', '1');
INSERT INTO `ss_node` VALUES ('2', '2', '1', 'hk2.gocyc.cc', 'rc4-md5', '0', '1', '123', '1', '0', '2');

-- ----------------------------
-- Table structure for ss_node_info_log
-- ----------------------------
DROP TABLE IF EXISTS `ss_node_info_log`;
CREATE TABLE `ss_node_info_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL,
  `uptime` float NOT NULL,
  `load` varchar(32) NOT NULL,
  `log_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ss_node_info_log
-- ----------------------------

-- ----------------------------
-- Table structure for ss_node_online_log
-- ----------------------------
DROP TABLE IF EXISTS `ss_node_online_log`;
CREATE TABLE `ss_node_online_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL,
  `online_user` int(11) NOT NULL,
  `log_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ss_node_online_log
-- ----------------------------

-- ----------------------------
-- Table structure for ss_order
-- ----------------------------
DROP TABLE IF EXISTS `ss_order`;
CREATE TABLE `ss_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `addnum` int(11) NOT NULL,
  `total` decimal(10,0) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `shop_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shop_duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ss_order
-- ----------------------------

-- ----------------------------
-- Table structure for ss_password_reset
-- ----------------------------
DROP TABLE IF EXISTS `ss_password_reset`;
CREATE TABLE `ss_password_reset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(32) NOT NULL,
  `token` varchar(128) NOT NULL,
  `init_time` int(11) NOT NULL,
  `expire_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ss_password_reset
-- ----------------------------

-- ----------------------------
-- Table structure for ss_shop
-- ----------------------------
DROP TABLE IF EXISTS `ss_shop`;
CREATE TABLE `ss_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `price` decimal(10,2) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `flow` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ss_shop
-- ----------------------------
INSERT INTO `ss_shop` VALUES ('1', '套餐一', '{\"name\":\"\\u5957\\u9910\\u4e00\",\"price\":\"20\",\"duration\":\"30\",\"bandwidth\":\"50\",\"bandwidthCheck\":\"true\",\"flow\":\"50\",\"flowCheck\":\"on\"}', '20.00', '30', '0', '1496591773', null, '50');
INSERT INTO `ss_shop` VALUES ('3', '套餐2', '{\"name\":\"\\u5957\\u9910\\u4e00\",\"price\":\"60\",\"duration\":\"90\",\"bandwidth\":\"100\",\"bandwidthCheck\":\"true\",\"flow\":\"500\",\"flowCheck\":\"on\"}', '60.00', '30', '0', '1496591773', null, '500');
INSERT INTO `ss_shop` VALUES ('5', '套餐3', '{\"name\":\"\\u5957\\u99103\",\"price\":\"220\",\"duration\":\"365\",\"bandwidth\":\"100M\",\"flow\":\"1024\",\"priceCheck\":\"false\",\"durationCheck\":\"false\",\"bandwidthCheck\":\"true\",\"flowCheck\":\"true\"}', '220.00', '365', '0', '1500201620', null, '1024');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(128) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(32) NOT NULL,
  `pass` varchar(64) NOT NULL,
  `passwd` varchar(16) NOT NULL,
  `t` int(11) NOT NULL DEFAULT '0',
  `u` bigint(20) NOT NULL,
  `d` bigint(20) NOT NULL,
  `transfer_enable` bigint(20) NOT NULL,
  `port` int(11) NOT NULL,
  `protocol` varchar(32) NOT NULL DEFAULT 'origin',
  `obfs` varchar(32) NOT NULL DEFAULT 'plain',
  `switch` tinyint(4) NOT NULL DEFAULT '1',
  `enable` tinyint(4) NOT NULL DEFAULT '1',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `last_get_gift_time` int(11) NOT NULL DEFAULT '0',
  `last_check_in_time` int(11) NOT NULL DEFAULT '0',
  `last_rest_pass_time` int(11) NOT NULL DEFAULT '0',
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `invite_num` int(8) NOT NULL DEFAULT '0',
  `is_admin` int(2) NOT NULL DEFAULT '0',
  `ref_by` int(11) NOT NULL DEFAULT '0',
  `expire_time` int(11) NOT NULL DEFAULT '0',
  `method` varchar(64) NOT NULL DEFAULT 'rc4-md5',
  `is_email_verify` tinyint(4) NOT NULL DEFAULT '0',
  `reg_ip` varchar(128) NOT NULL DEFAULT '127.0.0.1',
  `vip` int(11) DEFAULT '0',
  `package` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `port` (`port`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'chris@afox.cc', '33e3cb854d35b1f08c2dc2961deece87', 'wPb72T', '0', '0', '0', '5468323840', '1025', 'origin', 'plain', '1', '1', '1', '0', '1496331824', '0', '2017-05-31 14:38:02', '5', '1', '0', '1501084800', 'rc4-md5', '0', '127.0.0.1', '0', '1');
INSERT INTO `user` VALUES ('2', 'dd', 'dd@afox.cc', '33e3cb854d35b1f08c2dc2961deece87', 'NLVA3Q', '0', '0', '0', '0', '1026', 'origin', 'plain', '1', '1', '1', '0', '0', '0', '2017-07-16 09:48:26', '0', '0', '0', '1500217339', 'rc4-md5', '0', '192.168.10.1', '0', '2');

-- ----------------------------
-- Table structure for user_token
-- ----------------------------
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(256) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `expire_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_token
-- ----------------------------

-- ----------------------------
-- Table structure for user_traffic_log
-- ----------------------------
DROP TABLE IF EXISTS `user_traffic_log`;
CREATE TABLE `user_traffic_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u` int(11) NOT NULL,
  `d` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `rate` float NOT NULL,
  `traffic` varchar(32) NOT NULL,
  `log_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_traffic_log
-- ----------------------------
