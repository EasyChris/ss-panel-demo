/**
 * Created by chrischen on 2017/5/25.
 */
/**
 * Created by chrischen on 2017/5/20.
 */

/**
 * getQueryString(name)
 *
 * @param string n: length of
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
}

/**
 * Number.prototype.format(n, x)
 *
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

/**
 * formatTime(time)
 *
 * @param time : timestamp
 * @return : 2017-05-20 13:14:25
 */
function formatTime(time) {
    var date = new Date(time);
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;

    var str = date.getFullYear() + "-" + month + "-" + day + " " +  hour + ":" + min + ":" + sec;

    return str;
}

function getEle(_class) {
    return document.querySelector(_class);
}



//trim
function trim(value){
    var text = value ? value.replace(/(^\s+)|(\s+$)/g,'') : '';
    return text;
}


//判断是否为空
function is_empty(ele) {
    if (ele === "" || ele === null) {
        return false;
    } else {
        return true
    }
}